# Copyright (C) 2013-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
all::
rubybin := $(shell which ruby)
RUBY = $(rubybin)
lib := lib

# support using eatmydata to speed up tests (apt-get install eatmydata):
# https://www.flamingspork.com/projects/libeatmydata/
EATMYDATA =
-include config.mak

ifeq ($(RUBY_TEST_OPTS),)
  ifeq ($(V),1)
    RUBY_TEST_OPTS := -v
  endif
endif

all:: test
test_units := $(wildcard test/test_*.rb)
test: $(test_units)
$(test_units):
	$(EATMYDATA) $(RUBY) -w -I $(lib) $@ $(RUBY_TEST_OPTS)

check-warnings:
	@(for i in $$(git ls-files '*.rb'| grep -v '^setup\.rb$$'); \
	  do $(RUBY) -d -W2 -c $$i; done) | grep -v '^Syntax OK$$' || :

check: test

coverage: export COVERAGE=1
coverage:
	>coverage.dump
	$(MAKE) check
	$(RUBY) ./test/covshow.rb

.PHONY: all test $(test_units) NEWS
.PHONY: check-warnings fix-perms
