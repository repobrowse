# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'
require 'repobrowse/git_raw_tree_html'

class TestGitRawTreeHTML < Test::Unit::TestCase
  def test_foo
    git_tmp_repo do |tmp_dir, git_dir|
      g = Repobrowse::Git.new(git_dir)
      rgd = g.rugged
      re = %r{<li><a\nhref="[^"]+">([^<]+)</a></li>}

      tree = rgd.rev_parse('HEAD:')
      foo = Repobrowse::GitRawTreeHTML.new(tree, 'HEAD', nil)
      s = +''
      foo.each { |buf| s << buf }
      cmd = %W(git --git-dir=#{git_dir} ls-tree -z --name-only HEAD:)
      dir = IO.popen(cmd, 'rb') { |rd| rd.readlines("\0", chomp: true) }
      assert_equal dir, s.scan(re).flatten!
      foo.close

      tree = rgd.rev_parse('HEAD:dir')
      foo = Repobrowse::GitRawTreeHTML.new(tree, 'HEAD', 'dir')
      s = +''
      foo.each { |buf| s << buf }
      cmd = %W(git --git-dir=#{git_dir} ls-tree -z --name-only HEAD:dir)
      dir = IO.popen(cmd, 'rb') { |rd| rd.readlines("\0", chomp: true) }
      assert_equal [ '../', dir ].flatten!, s.scan(re).flatten!
      foo.close
    end
  end
end
