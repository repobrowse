# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'
require 'rss'

class TestGitAtom < Test::Unit::TestCase
  def test_git_atom
    git_tmp_repo do |tmp_dir, git_dir|
      s, h, b = req('GET', "/tmp/atom/master")
      assert_equal 'application/atom+xml', h['Content-Type']
      assert_equal 200, s.to_i
      str = body_string(b)
      feed = RSS::Parser.parse(str)
      assert_kind_of RSS::Atom::Feed, feed
      assert_predicate feed, :valid?

      s, h, _ = req('GET', "/tmp/atom/master:non/existent")
      assert h['Content-Type'], 'whatever type is fine for now'
      assert_equal 404, s.to_i

      s, h, _ = req('GET', "/tmp/atom/master:dir/")
      assert_equal 302, s.to_i
      assert_match %r{\Ahttp://[^/]+/tmp/atom/master:dir\z}, h['Location']
    end
  end
end
