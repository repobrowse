# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'

class TestGitShow < Test::Unit::TestCase
  def test_git_show
    git_tmp_repo do |tmp_dir, git_dir|
      g = Repobrowse::Git.new(git_dir)
      cmt = g.rugged.rev_parse_oid('HEAD')
      assert_match %r{\A[a-f0-9]{40}\z}, cmt
      status, _, body = req('GET', "/tmp/#{cmt}")
      assert_equal 200, status.to_i
      tidy_check(body)
    end
  end
end
