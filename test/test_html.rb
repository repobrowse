# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: false
require_relative 'helper'
require 'repobrowse/html'

class TestHtml < Test::Unit::TestCase
  def test_attr
    html = Repobrowse::HTML.new
    ary = [ 'Hello/World.pm', 'Zcat', 'hello world.c', 'Eléanor', '$at',
      'hello_world'
    ]
    ary.each do |s|
      attr = html.to_anchor(-s)
      assert_not_same s, attr
      assert_match %r{\A[a-zA-Z][\w:\.]+\z}, attr
      assert_equal %Q("#{attr}"), attr.encode(xml: :attr),
                  'no need to encode again'
      assert_equal s, html.from_anchor(-attr)
    end
  end
end
