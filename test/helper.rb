# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
$-w = $stdout.sync = $stderr.sync = Thread.abort_on_exception = true

# fork-aware coverage data gatherer, see also test/covshow.rb
if ENV["COVERAGE"]
  require "coverage"
  COVMATCH = %r{\brepobrowse\b}
  COVDUMPFILE = File.expand_path("coverage.dump")

  def __covmerge
    res = Coverage.result

    # do not create the file, Makefile does this before any tests run
    File.open(COVDUMPFILE, IO::RDWR) do |covtmp|
      covtmp.binmode
      covtmp.sync = true

      # we own this file (at least until somebody tries to use NFS :x)
      covtmp.flock(File::LOCK_EX)

      prev = covtmp.read
      prev = prev.empty? ? {} : Marshal.load(prev)
      res.each do |filename, counts|
        # filter out stuff that's not in our project
        COVMATCH =~ filename or next

        # For compatibility with https://bugs.ruby-lang.org/issues/9508
        # TODO: support those features if that gets merged into mainline
        unless Array === counts
          counts = counts[:lines]
        end

        merge = prev[filename] || []
        merge = merge
        counts.each_with_index do |count, i|
          count or next
          merge[i] = (merge[i] || 0) + count
        end
        prev[filename] = merge
      end
      covtmp.rewind
      covtmp.truncate(0)
      covtmp.write(Marshal.dump(prev))
      covtmp.flock(File::LOCK_UN)
    end
  end

  Coverage.start
  require 'test/unit'
  Test::Unit.at_exit { __covmerge }
else
  require 'test/unit'
end
require 'thread'
require 'fileutils'
require 'tempfile'
require 'tmpdir'
require 'webrick'
require 'repobrowse'

def git_tmp_repo
  Dir.mktmpdir do |tmp_dir|
    git_dir = "#{tmp_dir}/tmp.git"
    assert(system(*%W(git init -q --bare #{git_dir})), 'initialize git dir')
    assert(system(*%W(git --git-dir=#{git_dir} fast-import --quiet),
           in: "#{File.dirname(__FILE__)}/git.fast-import-data"),
           'fast-import test repo')
    projects = { 'repo.tmp.path' => git_dir }
    @app = Rack::Builder.new { run Repobrowse::App.new(projects) }.to_app
    @req = Rack::MockRequest.new(@app)
    yield tmp_dir, git_dir
  end
end

def rack_server(app)
  readyq = Queue.new
  logq = Queue.new
  host = '127.0.0.1'
  config = {
    Logger: WEBrick::Log.new(logq),
    app: app,
    Host: host,
    Port: 0, # WEBrick will choose a random port
    server: 'webrick',
  }
  config[:AccessLog] = [] unless test_verbose?

  th = Thread.new do
    srv = Rack::Server.new(config)
    srv.start { |s| readyq.push(s) }
  end
  s = readyq.pop
  assert_instance_of WEBrick::HTTPServer, s

  # grab the random port chosen by WEBrick
  case l = logq.pop
  when / port=(\d+)\n/
    port = $1.to_i
    break
  end while true

  Thread.new do
    while l = logq.pop
      warn "W: #{l.inspect}"
    end
  end if test_verbose?

  yield logq, host, port
ensure
  s&.shutdown
  th&.join
end

def test_verbose?
  ENV['TEST_VERBOSE'].to_i != 0
end

# Rack::MockRequest doesn't always work since it converts the
# response into an array without dup-ing Strings, so response
# bodies which reuse a buffer (when they assume they're writing
# to a socket) fail
def req(method, uri, opts = {})
  o = { method: method }.merge!(opts)
  env = @req.class.env_for(uri, o)
  @app.call(env)
end

def body_string(body)
  rv = ''.b
  body.each { |chunk| rv << chunk }
  rv
ensure
  body.close if body.respond_to?(:close)
end

$tidy = nil
def tidy_check(input)
  if $tidy.nil?
    case v = `tidy --version 2>/dev/null`
    when /version (\d+)\.\d+\.\d+/ # HTML Tidy for Linux/x86 version 5.2.0
      major = $1.to_i
      $tidy = major
    when ''
      $tidy = false
    else # "HTML Tidy for Linux released on 25 March 2009"
      $tidy = 4
    end
  end
  if $tidy == false
    begin
      body.each { |buf| assert_instance_of(String, buf) }
    ensure
      body.close
    end
  else
    rd, wr = IO.pipe
    th = Thread.new(input) do |body|
      begin
        first = true
        body.each do |chunk|
          if first && $tidy < 5 # HTML5 allows optional type
            first = false
            chunk = chunk.sub(/<style>/, '<style type="application/css">')
          end
          wr.write(chunk)
        end
      ensure
        wr.close
        body.close
      end
    end
    pid = spawn("tidy -q", in: rd, out: IO::NULL)
    _, status = Process.waitpid2(pid)
    assert_predicate status, :success?, status.inspect
  end
ensure
  th&.join
  rd&.close
  wr&.close
end
