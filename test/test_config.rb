# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'

class TestConfig < Test::Unit::TestCase
  def test_load_hash
    config = {
      'repo.git.path' => '/path/to/git.git',
      'repo.repobrowse.path' => '/path/to/repobrowse.git',
    }
    config = Repobrowse::Config.new(config)
    assert_instance_of Repobrowse::Config, config
  end

  def test_load_file
    cfg = Tempfile.new('git-config')
    cfg.write <<''
[repo "git"]
	email = a@example.com
	email = b@example.com
	path = /path/to/nowhere

    cfg.flush
    cfg = Repobrowse::Config.new(cfg.path)
    assert_equal %w(a@example.com b@example.com),
      cfg.instance_variable_get(:@raw)['repo.git.email']
  end
end
