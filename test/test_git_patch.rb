# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'

class TestGitPatch < Test::Unit::TestCase
  def test_git_patch
    git_tmp_repo do |tmp_dir, git_dir|
      g = Repobrowse::Git.new(git_dir)
      cmt = g.rugged.rev_parse('HEAD')
      cmt = cmt.oid
      assert_match %r{\A[a-f0-9]{40}\z}, cmt
      status, headers, body = req('GET', "/tmp/#{cmt}.patch")
      assert headers.delete('Expires'), 'Expires exists'
      orig = [ status, headers ].freeze
      assert_equal 200, status.to_i
      assert_equal 'text/plain; charset=UTF-8', headers['Content-Type']
      assert_equal %Q{inline; filename="#{cmt}.patch"},
                    headers['Content-Disposition']
      str = body_string(body)
      assert_match %r{\AFrom #{cmt} }, str

      patch = str.split("\n")
      sig = patch.pop
      assert(sig.sub!(/format-patch /, 'format-patch --no-signature '))
      assert_equal '-- ', patch.pop
      sig_out = IO.popen(sig, chdir: git_dir, &:read)
      sig_out = sig_out.split("\n")
      assert_equal patch, sig_out, 'sig used to regenerate the patch'

      status, headers, body = req('GET', "/tmp/#{cmt[0,16]}.patch")
      assert_equal 302, status, 'redirected to full commit URL'
      assert_match %r{://[^/]+/tmp/#{cmt}\.patch\z}, headers['Location']

      res = req('HEAD', "/tmp/#{cmt}.patch")
      assert res[1].delete('Expires'), 'Expires exists'
      assert_equal '', body_string(res.pop), 'HEAD gives empty body'
      assert_equal orig, res, 'HEAD works'

      res = req('GET', "/tmp/#{'0' * 40}.patch")
      assert_equal 404, res[0].to_i
    end
  end
end
