# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'
require 'repobrowse/git'

class TestGit < Test::Unit::TestCase
  def test_unquote
    g = Repobrowse::Git.new('/dev/null')
    assert_equal "foo\nbar", g.git_unquote('"foo\\nbar"'), 'unquoted newline'
    assert_equal "Eléanor", g.git_unquote('"El\\303\\251anor"'),
      'unquoted octal'
  end
end
