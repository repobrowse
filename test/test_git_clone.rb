# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'helper'

class TestClone < Test::Unit::TestCase
  def test_clone
    git_tmp_repo do |tmp_dir, git_dir|
      rack_server(@app) do |logq, host, port|
        url = "http://#{host}:#{port}/tmp"
        dst = "#{tmp_dir}/dst"

        assert system(*%W(git clone -q #{url} #{dst})),
            'smart clone works'
        FileUtils.rm_rf(dst)

        assert system(*%W(git --git-dir=#{git_dir} update-server-info)),
            'enable dumb clones'

        env = { 'GIT_SMART_HTTP' => '0' }
        assert system(env, *%W(git clone -q #{url} #{dst})),
            'dumb clone works'
        FileUtils.rm_rf(dst)

        assert system(*%W(git --git-dir=#{git_dir} repack -adq)), 'repack'
        assert system(env, *%W(git clone -q #{url} #{dst})),
            'dumb clone works after packing'

        packs = Dir["#{git_dir}/objects/pack/*.pack"]
        if packs[0] =~ %r{(/objects/pack/[^\.]+\.pack)\z}
          require 'net/http'
          Net::HTTP.start(host, port) do |http|
            path = -"/tmp#$1"
            res = http.request(Net::HTTP::Get.new(path))
            pack = File.open(packs[0], 'rb', &:read)
            size = pack.size
            assert_equal pack, res.body
            assert_equal 200, res.code.to_i

            req = Net::HTTP::Get.new(path, 'Range' => 'bytes=5-46')
            res = http.request(req)
            assert_equal 206, res.code.to_i
            assert_equal "bytes 5-46/#{size}", res['Content-Range']
            assert_equal pack[5..46], res.body
            assert_equal 'application/x-git-packed-objects', res['Content-Type']

            req = Net::HTTP::Get.new(path, 'Range' => 'bytes=5-')
            res = http.request(req)
            assert_equal 206, res.code.to_i
            last = size - 1
            assert_equal "bytes 5-#{last}/#{size}", res['Content-Range']
            assert_equal pack[5..-1], res.body

            req = Net::HTTP::Get.new(path, 'Range' => 'bytes=-1')
            res = http.request(req)
            assert_equal 206, res.code.to_i
            assert_equal "bytes #{last}-#{last}/#{size}", res['Content-Range']
            assert_equal pack[-1], res.body

            req = Net::HTTP::Get.new(path, 'Range' => 'bytes=0-0')
            res = http.request(req)
            assert_equal 206, res.code.to_i
            assert_equal "bytes 0-0/#{size}", res['Content-Range']
            assert_equal pack[0], res.body
          end
        else
          warn "no packs? #{packs.inspect}"
        end
      end
    end
  end
end
