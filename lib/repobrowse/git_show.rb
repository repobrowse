# -*- encoding: utf-8 -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

require_relative 'git_commit_html'

module Repobrowse::GitShow

  def show(r, repo, objid)
    rgd = repo.driver.rugged
    begin
      oid = rgd.rev_parse_oid(objid)
    rescue
      return r404(r)
    end
    # disambiguate, expand abbreviated commit IDs to be cache-friendly
    if oid != objid
      return r.redirect("#{r.base_url}#{r.script_name}/#{repo.name}/#{oid}")
    end
    hdr = rgd.read_header(oid)
    case hdr[:type]
    when :commit
      html = Repobrowse::GitCommitHTML.new
      html.commit_header(r.env, repo, rgd.lookup(oid))
      r.halt html.response(200)
    # TODO: other types
    else
      r404(r)
    end
  end
end
