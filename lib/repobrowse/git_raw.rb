# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

require_relative 'escape'
require_relative 'git_raw_tree_html'
require_relative 'git_disambiguate'
require_relative 'pipe_body'

module Repobrowse::GitRaw
  include Repobrowse::GitDisambiguate

  # /$REPO_NAME/raw/$REF:$PATH
  def raw(r, repo, ref, path)
    git_disambiguate(r, repo, 'raw', ref, path)
    h = { 'Content-Type' => 'text/plain; charset=UTF-8' }
    spec = "#{ref}:#{path}"
    rgd = rugged
    begin
      oid = rgd.rev_parse_oid(spec)
      hdr = rgd.read_header(oid)
    rescue
      return r404(r)
    end
    type = hdr[:type]
    if type == :tree
      body = Repobrowse::GitRawTreeHTML.new(rgd.lookup(oid), ref, path)
      h['Content-Type'] = 'text/html; charset=UTF-8'
    else
      fn = path.split('/')[-1]
      h['ETag'] = %Q{"#{oid}"}
      len = hdr[:len]
      h['Content-Length'] = len.to_s
      h['Content-Disposition'] = %Q{inline; filename="#{fn}"}
      if len < 512 * 1024
        body = rgd.lookup(oid).content
        peek = body[0, 8000]
        body = [ body ]
      else # large objects
        io = repo.driver.popen(%Q(cat-file -t #{type} #{oid}))
        peek = io.read(8000) or return r404(r)
        body = Repobrowse::PipeBody.new(io, peek)
      end
      peek.include?("\0") and
        h['Content-Type'] = 'application/octet-stream'
    end
    r.halt [ 200, h, body ]
  end
end
