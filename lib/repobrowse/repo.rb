# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
#
# class for represpenting an inbox git repository.
class Repobrowse::Repo
  attr_reader :driver
  attr_reader :name
  attr_reader :path
  attr_reader :lineno_prefix

  def initialize(opts)
    @lineno_prefix = 'n' # 'l' for gitweb conversions
    opts.each { |k, v| instance_variable_set "@#{k}", v }
    case @vcs
    when 'git'
      @driver = Repobrowse::Git.new(@path)
    end
  end

end
