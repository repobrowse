# -*- encoding: utf-8 -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

require_relative 'escape'

# used to give each HTML page a consistent look, this is a Rack response body
class Repobrowse::HTML
  attr_reader :buf # meant for appending to via String#<<

  include Repobrowse::Escape

  def initialize
    @buf = nil
  end

  def start(title_html, desc, robots: nil)
    meta = %Q(<meta\nname=robots\ncontent="#{robots}" />) if robots
    @buf = +<<~EOF
      <!DOCTYPE html>
      <html><head><title>#{
        title_html
      }</title><style>pre{white-space:pre-wrap}</style>#{
        meta
      }</head><body><pre><b>#{
        case desc
        when String, nil
          desc
        else
          desc.driver.description.encode(xml: :text)
        end
      }</b>
    EOF
  end

  ESCAPES = { '/' => ':' }
  (0..255).each { |x|
    key = -x.chr
    next if key =~ %r{\A[\w\.\-/]}n
    ESCAPES[key] = -sprintf('::%02x', x)
  }

  def to_anchor(str)
    str = str.dup
    first = ''
    # must start with alphanum
    str.sub!(/\A([^A-Ya-z])/n, '') and first = sprintf('Z%02x', $1.ord)
    str.gsub!(/([^\w\.\-])/n, ESCAPES)
    "#{first}#{str}"
  end

  def from_anchor(str)
    str = str.dup
    first = ''
    str.sub!(/\AZ([a-f0-9]{2})/n, '') and first = -$1.hex.chr
    str.gsub!(/::([a-f0-9]{2})/n) { $1.hex.chr }
    str.tr!(':', '/')
    "#{first}#{str}"
  end

  def response(code, headers = {})
    headers['Content-Type'] ||= 'text/html; charset=UTF-8'
    [ 200, headers, self ]
  end
end
