# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

require 'time'

# provides smart HTTP cloning for git repos
module Repobrowse::GitHTTPBackend

  # used to provide streaming Rack response body
  class EachWrap
    def initialize(io)
      @io = io
    end

    def each
      begin
        buf = @io.readpartial(16384, buf)
        yield buf
      rescue EOFError
        return
      ensure
        buf&.clear
      end while true
    end

    def close
      @io.close
    end
  end

  def input_to_file(env)
    tmp = Tempfile.new('git-http-backend-in')
    tmp.unlink
    tmp.sync = true
    IO.copy_stream(env['rack.input'], tmp.to_io)
    tmp.rewind
    tmp
  end

  def run_http_backend(r, repo, path)
    penv = {
      'GIT_HTTP_EXPORT_ALL' => '1',
      'PATH_TRANSLATED' => -"#{repo.path}/#{path}",
    }
    env = r.env
    # GIT_COMMITTER_NAME, GIT_COMMITTER_EMAIL
    # may be set in the server-process and are passed as-is
    %w(QUERY_STRING REMOTE_USER REMOTE_ADDR HTTP_CONTENT_ENCODING
       CONTENT_TYPE SERVER_PROTOCOL REQUEST_METHOD).each do |k|
      v = env[k] and penv[k] = v
    end
    IO.popen(penv, %W(git http-backend), in: input_to_file(env))
  rescue => e
    r_err(r, "E: #{e.message} (#{e.class}) on #{repo.path}")
  end

  # returns undef if 403 so it falls back to dumb HTTP
  def serve_smart(r, repo, path)
    rd = run_http_backend(r, repo, path)
    code = 200
    h = {}

    # parse CGI response headers
    case rd.gets
    when %r{\AStatus:\s+(\d+)}i
      code = $1.to_i
    when %r{\A([^:]+):\s*(.*)\r\n\z}
      h[-$1] = -$2
    when "\r\n"
      break # headers done onto the body
    when nil
      rd.close
      r_err(r, "unexpected EOF on git http-backend in #{repo.path}")
    end while true

    r.halt [ code, h, EachWrap.new(rd) ]
  end

  def git_http_backend_routes(r, repo)
    r.is(path = 'git-upload-pack') { r.post { serve_smart(r, repo, path) } }
    r.is(path = 'info/refs') {
      # QUERY_STRING has exactly one parameter
      # See https://80x24.org/git/src/v2.16.1:Documentation/technical/http-protocol.txt
      if /\Aservice=git-\w+-pack\z/ =~ r.env['QUERY_STRING']
        r.get { serve_smart(r, repo, path) }
      else
        static(r, -"#{repo.path}/#{path}", 'text/plain', nil)
      end
    }
    %w(HEAD cloneurl description
       objects/info/http-alternates
       objects/info/alternates
       objects/info/packs).each { |txt|
      r.is(txt) { static(r, -"#{repo.path}/#{txt}", 'text/plain', nil) }
    }
    r.is('objects', :git_x2, :git_x38) { |x2, x38|
      static(r, -"#{repo.path}/objects/#{x2}/#{x38}",
             'application/x-git-loose-object')
    }
    [ :git_pack, 'application/x-git-packed-objects',
      :git_pack_idx, 'application/x-git-packed-objects-toc'
    ].each_slice(2) { |sym, type|
      r.is('objects/pack', :sym) { |o|
        static(r, -"#{repo.path}/objects/pack/#{o}", type)
      }
    }
  end
end
