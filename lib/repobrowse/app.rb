# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'error'
require_relative 'static'
require_relative 'git_http_backend'
require_relative 'config'
require 'roda'

module Repobrowse::App

  # main entry point for rackup config.ru files
  def self.new(config)
    case config
    when Hash, String, nil
      config = Repobrowse::Config.new(config)
    end

    # allow multiple instances of our app:
    c = Class.new(Roda)
    c.class_eval do
      include Repobrowse::Error
      include Repobrowse::Static
      include Repobrowse::GitHTTPBackend
      plugin :symbol_matchers
      plugin :streaming
      plugin :head

      symbol_matcher :git_pack, %r{(pack-[a-f0-9]{40,}\.pack)}
      symbol_matcher :git_pack_idx, %r{(pack-[a-f0-9]{40,}\.idx)}
      symbol_matcher :git_x2, %r{([a-f0-9]{2})}
      symbol_matcher :git_x38, %r{([a-f0-9]{38,})}
      symbol_matcher :patch, %r{([a-f0-9]{7,})\.patch}
      symbol_matcher :show, %r{([a-f0-9]{7,})}
      symbol_matcher :tree_path, %r{([^:]+)(?::(.+))?}

      route do |r|
        config.repos.each_value do |repo|
          r.on(repo.name) do
            git_http_backend_routes(r, repo)
            r.is { r.get { repo.driver.summary(r, repo) } }
            r.is('src', :tree_path) { |ref, path|
              r.get { repo.driver.src(r, repo, ref, path) }
            }
            r.is('raw', :tree_path) { |ref, path|
              r.get { repo.driver.raw(r, repo, ref, path) }
            }
            r.is(:show) { |x| r.get { repo.driver.show(r, repo, x) } }
            r.is(:patch) { |x| r.get { repo.driver.patch(r, repo, x) } }
            r.is('atom', :tree_path) { |ref, path|
              r.get { repo.driver.atom(r, repo, ref, path) }
            }
          end # r.on
        end # projects.each
      end # ret.route
    end
    c.freeze.app
  end # self.new
end
