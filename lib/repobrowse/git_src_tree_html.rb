# -*- encoding: utf-8 -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'html'
require 'uri'

class Repobrowse::GitSrcTreeHTML < Repobrowse::HTML
  def initialize(tree, ref, path, repo)
    super()
    @tree = tree
    @repo = repo
    ref_parts = ref.split('/')
    ref_uri = ref_parts.map { |s| CGI.escape(s) }
    ref_html = ht(ref.dup)
    if path
      path_parts = path.split('/')
      path_uri = path_parts.map { |s| CGI.escape(s) }
      path_html = ht(path.dup)
      path_uri[0] = "#{ref_uri[-1]}:#{path_uri[0]}" if path_uri[0]
      relup = (2..path_parts.size).map { '../' }.join
      base = path_parts.pop
      base_uri = path_uri.pop
      tmp = []
      path_parts.map! do |x|
        tmp << path_uri.shift
        href = ha(+"#{relup}#{tmp.join('/')}")
        %Q(<a\nhref=#{href}>#{ht(x)}</a>)
      end
      path_parts << "<b>#{ht(base)}</b>"
      @rel_prefix = "./#{base_uri}/"
    else
      path_parts = [ '[root]' ]
      relup = ''
      @rel_prefix = "./#{ref_uri[-1]}:"
    end
    @buf = start("#{ref_html}:#{path_html}", repo)
    @buf << <<EOS
<a
href=#{ha(relup + ref_uri[-1])}>#{ref_html}</a> : #{path_parts.join(' / ')}

tree #{@tree.oid}</pre><hr/><pre>
EOS
    @buf.chomp!
  end

  def fmt_ent(ent, rgd, parts)
    name = ent[:name]
    name_text = ht(name.dup)
    case ent[:filemode]
    when 0100644 # plain blob, nothing special
      md = ' '
    when 0040000
      return do_descend(ent, rgd) unless parts
      md = 'd'
      len = '-'
    when 0100755
      md = 'x'
      name_text = "<b>#{name_text}</b>"
    when 0120000
      md = 'l'
      name_text = "<i>#{name_text}</i>"
    when 0160000
      md = 'g'
      name_text = "#{name_text} <u>@ #{ent[:oid]}</u>"
      len = '-'
    else
      warn "unknown mode: #{'%06o' % ent[:filemode]}"
    end
    len ||= rgd.read_header(ent[:oid])[:len].to_s
    if parts && dir = parts.shift
      href = @rel_prefix + URI.escape(dir)
      dir = "<a\nhref=#{ha(href.dup)}>#{ht(dir)}</a>"
      parts.map! do |part|
        href << "/#{URI.escape(part)}"
        "<a\nhref=#{ha(href.dup)}>#{ht(part)}</a>"
      end
      parts.unshift(dir)
      href << "/#{URI.escape(name)}"
      parts << "<a\nhref=#{ha(href)}>#{name_text}</a>"
      rest = parts.join(' / ')
    else
      rest = "<a\nhref=#{ha(@rel_prefix + URI.escape(name))}>#{name_text}</a>"
    end
    "#{md} #{sprintf('% 8s', len)}\t#{rest}\n"
  end

  def do_descend(ent, rgd)
    parts = []
    begin
      tree = rgd.lookup(ent[:oid])
      if tree.count != 1
        break
      else
        parts << ent[:name]
        ent = tree.get_entry(0)
        ent[:filemode] == 0040000 or break
      end
    end while true
    fmt_ent(ent, rgd, parts)
  end

  # called by the Rack server
  def each
    yield @buf
    @buf.clear
    rgd = @repo.driver.rugged
    @tree.each do |ent|
      yield fmt_ent(ent, rgd, nil)
    end
    yield '</pre></body></html>'
    self
  end

  # called by the Rack server
  def close
  end
end
