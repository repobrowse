# -*- encoding: utf-8 -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'html'
require 'uri'

class Repobrowse::GitSrcBlobHTML < Repobrowse::HTML
  def initialize(hdr, oid, ref, path, repo)
    super()
    @repo = repo
    ref_parts = ref.split('/')
    ref_uri = ref_parts.map { |s| CGI.escape(s) }
    ref_html = ht(ref.dup)
    path_parts = path.split('/')
    path_uri = path_parts.map { |s| CGI.escape(s) }
    path_html = ht(path.dup)
    relup = (2..(ref_parts.size + path_parts.size)).map { '../' }.join
    raw = +"#{relup}/raw/#{ref_uri.join('/')}:#{path_uri.join('/')}"
    path_uri[0] = "#{ref_uri[-1]}:#{path_uri[0]}" if path_uri[0]
    relup = (2..path_parts.size).map { '../' }.join
    base = path_parts.pop
    _base_uri = path_uri.pop
    tmp = []
    path_parts.map! do |x|
      tmp << path_uri.shift
      href = +"#{relup}#{tmp.join('/')}"
      %Q(<a\nhref=#{ha(href)}>#{ht(x)}</a>)
    end
    path_parts << "<b>#{ht(base)}</b>"
    @buf = start("#{ref_html}:#{path_html}", repo)
    len = hdr[:len]
    @buf << <<EOS
<a
href=#{ha(relup + ref_uri[-1])}>#{ref_html}</a> : #{path_parts.join(' / ')}

blob #{oid}\t#{len} bytes (<a
href=#{ha(raw)}>raw</a>)</pre><hr/>
EOS
    @buf.chomp!
    if len < (512 * 1024)
      @oid = oid
    end
  end

  # Called by the rack server
  def each
    if @buf
      yield @buf
      @buf.clear
      @buf = nil
    end

    # TODO: optional syntax highlighting
    # highlight(1) supports streaming; but needs be spawned
    # highlight should also be familiar to gitweb and cgit users
    # rouge allows streaming output, but not input :<
    # coderay doesn't seem to support streaming at all
    if @oid
      body = @repo.driver.rugged.lookup(@oid).content
      if body[0, 8000].include?("\0")
        yield "<pre>Binary file, save using the 'raw' link above</pre>"
      else
        yield '<table><tr><td><pre>'
        n = body.count("\n")
        yield ht(body)
        unless body.end_with?("\n")
          n += 1
          yield "\n\\ No newline at end of file"
        end
        yield '</pre></td><td><pre>'
        pfx = @repo.lineno_prefix
        (1..n).each do |i|
          anchor = "#{pfx}#{i}"
          yield %Q(<a\nid=#{anchor}\nhref="##{anchor}">#{i}</a>\n)
        end
        yield '</pre></td></tr></table>'
      end
    else
      yield "<pre>File is too big to display, save using the 'raw' link</pre>"
    end
    yield '</body></html>'
    self
  end

  # called by the Rack server
  def close
  end
end
