# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'escape'

# ugly "raw" tree view, "git_src_tree_html" has the prettier one
class Repobrowse::GitRawTreeHTML
  include Repobrowse::Escape

  def initialize(tree, ref, path)
    @tree = tree
    @ref = ref
    @path = path # nil ok
  end

  def each
    if @path
      t = ht(+"/#@path/")
      path = @path.split('/')
      case path.size
      when 1
        ref = CGI.escape(@ref.split('/')[-1])
        pfx = "./#{ref}:#{CGI.escape(path[0])}/"
        up = ha('./' + ref)
      else
        pfx = "#{CGI.escape(path[-1])}/"
        up = "#@ref:#@path".split('/')[-2]
        up = up.split(':').map! { |u| CGI.escape(u) }.join(':')
        up = ha('../' + up)
      end
    else
      t = '/'
      pfx = "./#{CGI.escape(@ref.split('/')[-1])}:"
    end
    yield "<html><head><title>#{t}</title></head><body><h2>#{t}</h2><ul>"
    yield %Q(<li><a\nhref=#{up}>../</a></li>) if up
    @tree.each do |ent|
      name = ent[:name]
      href = ha(pfx + CGI.escape(name))
      name = ht(name)
      yield %Q(<li><a\nhref=#{href}>#{name}</a></li>)
    end
    yield '</ul></body></html>'
    self
  end

  def close
  end
end
