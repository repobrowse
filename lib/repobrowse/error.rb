# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

module Repobrowse::Error
  def r_err(r, msg)
    r.env['rack.logger']&.error(msg)
    b = "Internal server error\n"
    r.halt [ 500, { 'Content-Type' => 'text/plain; charset=UTF-8' }, [ b ] ]
  end

  def r404(r)
    b = "Not Found\n"
    h = {
      'Content-Type' => -'text/plain; charset=UTF-8',
      'Content-Length' => -(b.size.to_s),
    }
    r.halt [ 404, h, [ b ] ]
  end
end
