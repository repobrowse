# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
module Repobrowse::GitDisambiguate
  def git_disambiguate(r, repo, pfx, ref, path)
    ambiguous = false
    ambiguous = true if ref.squeeze!('/')
    ambiguous = true if ref.chomp!('/')
    if path
      ambiguous = true if path.chomp!('/')
      ambiguous = true if path.squeeze!('/')
    end
    if ambiguous
      loc = r.base_url
      loc << "#{r.script_name}/#{repo.name}/#{pfx}/#{ref}"
      loc << ":#{path}" if path
      r.redirect(loc) # throws
    end
  end
end
