# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

module Repobrowse::GitAtom
  DATEFMT = '%Y-%m-%dT%H:%M:%SZ'

  # running git-log(1) here for now, path-limiting with libgit2|rugged
  # seems like a pain to implement
  ATOM_ARGV = %W(log --no-notes --no-color --no-abbrev --encoding=UTF-8
                 --pretty=tformat:%#{%w(H s ct an ae at b).join('%n%')}%x00)

  class Body
    include Repobrowse::Error
    include Repobrowse::Escape

    def initialize(rd, r, repo, ref, path)
      @rd = rd
      title = xt(+"#{repo.name} #{ref} #{path || '/'}")
      subtitle = xt(repo.driver.description.dup)
      @url = "#{r.base_url}#{r.script_name}/#{repo.name}"
      # read the first entry to ensure we have something
      updated = +''
      tmp = entry_begin(updated) or r404(r)

      @buf = +<<~EOF
        <?xml version="1.0"?><feed
        xmlns="http://www.w3.org/2005/Atom"><title>#{
        title}</title><subtitle>#{
        subtitle}</subtitle><link
        rel="alternate"
        type="text/html"
        href=#{xa(@url.dup)}
        /><id>#{
          xt("#@url/log/" + ref)
        }</id><updated>#{updated}</updated>
      EOF

      @buf << tmp
    end

    def entry_begin(updated = +'')
      x40 = @rd.gets(chomp: true) or return
      +<<~EOF
        <entry><title>#{ # %s
          xt(@rd.gets(chomp: true))
        }</title><updated>#{ # %ct
          updated.replace(Time.at(@rd.gets.to_i).strftime(DATEFMT))
        }</updated><author><name>#{ # %an
          xt(@rd.gets(chomp: true))
        }</name><email>#{ # %ae
          xt(@rd.gets(chomp: true))
        }</email></author><published>#{ # %at
          Time.at(@rd.gets.to_i).strftime(DATEFMT)
        }</published><link
        rel="alternate"
        type="text/html"
        href=#{xa("#@url/" + x40)
        }/><id>#{x40}</id><content
        type="xhtml"><div
        xmlns="http://www.w3.org/1999/xhtml"><pre
        style="white-space:pre-wrap">
      EOF
    end

    def entry_body
      tmp = @rd.gets("\0\n", chomp: true)
      tmp.strip!
      xt(tmp)
    end

    def each
      entry_end = '</pre></div></content></entry>'
      tmp = @buf
      @buf = nil
      yield tmp
      tmp.clear
      tmp = entry_body
      yield tmp
      tmp.clear
      yield entry_end
      while tmp = entry_begin
        yield tmp
        tmp.clear
        tmp = entry_body
        yield tmp
        tmp.clear
        yield entry_end
      end
      yield '</feed>'
    end

    def close
      @rd.close
    end
  end

  # /$REPO_NAME/atom/$REF:PATH
  def atom(r, repo, ref, path)
    git_disambiguate(r, repo, 'atom', ref, path)
    max = 50
    argv = [ *ATOM_ARGV, *%W(-#{max} #{ref} --) ]
    argv << path if path
    rd = repo.driver.popen(argv)
    body = Body.new(rd, r, repo, ref, path)
    r.halt([ 200, { 'Content-Type' => 'application/atom+xml' }, body ])
  end
end
