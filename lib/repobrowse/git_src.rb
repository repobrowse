# -*- encoding: utf-8 -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true

require_relative 'git_src_blob_html'
require_relative 'git_src_tree_html'
require_relative 'git_disambiguate'

module Repobrowse::GitSrc
  include Repobrowse::GitDisambiguate

  MAX = 1024 * 1024

  # /$REPO_NAME/src/$REF:$PATH
  def src(r, repo, ref, path)
    git_disambiguate(r, repo, 'src', ref, path)
    spec = "#{ref}:#{path}"
    rgd = rugged
    begin
      oid = rgd.rev_parse_oid(spec)
      hdr = rgd.read_header(oid)
    rescue => e
      warn e.message
      return r404(r)
    end
    h = { 'ETag' => %Q{"#{oid}"} }
    case hdr[:type]
    when :tree
      tree = rgd.lookup(oid)
      html = Repobrowse::GitSrcTreeHTML.new(tree, ref, path, repo)
    when :blob
      html = Repobrowse::GitSrcBlobHTML.new(hdr, oid, ref, path, repo)
    end
    r.halt(html.response(200, h))
  end
end
