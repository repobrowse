# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require_relative 'repo'
require_relative 'git'

class Repobrowse::Config
  attr_reader :repos

  def initialize(file = ENV['REPOBROWSE_CONFIG'] ||
                        File.expand_path('~/.repobrowse/config'))
    @groups = Hash.new { |h,k| h[k] = [] }
    if Hash === file
      raw = file
    else
      raw = {}
      if File.exist?(file)
        cfg = Rugged::Config.new(file)
        cfg.each_key { |k| raw[-k] = cfg.get_all(k) }
      end
    end
    raw[:snapshots] ||= '-tar.bz2 -tar.xz'
    raw[:groups] = { hidden: [], none: [] }
    @raw = raw
    @repos = {}
    raw.each do |k, v|
      v = Array(v)
      case k
      when %r{\Arepo\.(.+)\.path\z}
        repo_name = -$1
        warn "multiple values defined for #{k.inspect}\n" if v.size > 1
        path = v[-1]
        add_repo(repo_name, path)
      end
    end
  end

  def add_repo(repo_name, path) # repo_name "git.git"
    rv = { path: path, name: repo_name }
    rv[:snapshot_pfx] = -path.split(%r{/+})[-1].chomp('.git')
    %i[ vcs readme group snapshots ].each do |key|
      rv[key] = @raw["repo.#{repo_name}.#{key}"]
    end
    rv[:snapshots] ||= @raw[:snapshots]
    rv[:snapshots_disabled] = disabled = {}
    rv[:snapshots].split(/\s+/n).each do |type|
      # "-tar.bz2", "-" prefix denotes disabled
      type.sub!(/\A-/, '') and
        disabled[type] = true
    end
    group = rv[:group] ||= :none
    case group
    when Array
      Array(group).each { |g| @groups[g] << repo_name }
    else
      @groups[group] << repo_name
    end
    snap = repo_name.split('/')[-1]
    if (rv[:vcs] ||= 'git') == 'git'
      snap.chomp!('.git') # seems common for git URLs to end in ".git"
    end
    rv[:snapshot_re] = %r{\A\Q#{Regexp.escape(snap)}[-_]}
    @repos[repo_name] = Repobrowse::Repo.new(rv)
  end
end
