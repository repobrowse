# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
#
require_relative 'git_show'
require_relative 'git_patch'
require_relative 'git_raw'
require_relative 'git_src'
require_relative 'git_atom'
require 'rugged'

class Repobrowse::Git
  GIT_ESC = {
    '\\a' => "\a",
    '\\b' => "\b",
    '\\f' => "\f",
    '\\n' => "\n",
    '\\r' => "\r",
    '\\t' => "\t",
    '\\v' => "\013",
  }.freeze

  def initialize(git_dir)
    @git_dir = -git_dir
  end

  def rugged
    Thread.current["rgd.#@git_dir".to_sym] ||= Rugged::Repository.new(@git_dir)
  end

  def description
    s = File.open("#@git_dir/description", &:read)
    s.strip!
    -s
  rescue
    '[error reading $GIT_DIR/description]'
  end

  def cloneurl
    File.open("#@git_dir/cloneurl", &:readlines).map! { |s| s.strip!; -s }
  rescue
    '[error reading $GIT_DIR/cloneurl]'
  end

  def popen(argv, encoding: Encoding::ASCII_8BIT)
    cmd = %w(git).concat(argv)
    IO.popen(cmd, chdir: @git_dir, encoding: encoding)
  end

  def tip
    rugged.head.name
  rescue
    '[error running git symbolic-ref --short HEAD]'
  end

  def git_unquote(s)
    return s unless s =~ /\A"(.*)"\z/n
    s = $1
    s.gsub!(/\\[abfnrtv]/n, GIT_ESC)
    s.gsub!(/\\([0-7]{1,3})/n) { $1.oct.chr }
    s
  end

  include Repobrowse::Error
  include Repobrowse::GitShow
  include Repobrowse::GitPatch
  include Repobrowse::GitRaw
  include Repobrowse::GitAtom
  include Repobrowse::GitSrc
end
