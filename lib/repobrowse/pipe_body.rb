# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
class Repobrowse::PipeBody
  attr_reader :to_io

  def initialize(io, buf)
    @to_io = io
    @buf = buf
  end

  # called by Rack server
  def each
    begin
      yield @buf
    end while @to_io.read(0x4000, @buf)
  end

  # called by Rack server
  def close
    @buf.clear
    @to_io.close
  end
end
