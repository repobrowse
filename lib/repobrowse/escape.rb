# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: true
require 'cgi'

module Repobrowse::Escape
  # like format_sanitized_subject in git.git pretty.c with '%f' format string
  def to_filename(str)
    str = str.sub(/\n.*\z/s, '')
    str.gsub!(/[^A-Za-z0-9_\.]+/, '-')
    str.squeeze!('.')
    str.sub!(/[\.\-]+\z/, '')
    str.sub!(/\A[\.\-]+/, '')
    str
  end

  def xt(str)
    str.encode!(xml: :text)
  end

  def xa(str)
    str.encode!(xml: :attr)
  end

  alias ha xa
  alias ht xt
end
