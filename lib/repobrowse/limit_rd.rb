# -*- encoding: binary -*-
# Copyright (C) 2017-2018 all contributors <repobrowse-public@80x24.org>
# License: AGPL-3.0+ <https://www.gnu.org/licenses/agpl-3.0.txt>
# frozen_string_literal: false

class Repobrowse::LimitRd
  def initialize(rd, size, buf, &blk)
    @rd = rd # git cat-file --batch output
    @left = size == 0 ? nil : size
    @buf = buf
    @on_close = blk
    @peek = nil
  end

  # called by Rack server in ensure
  def close
    @on_close&.call(self) # allows our @rd to be reused
    @buf&.clear
    @peek&.clear
    @on_close = nil
  end

  # used to determine if a file is binary or text
  def peek(len = 8000) # 8000 bytes is the same size used by git
    @peek = read(len, @buf)
  end

  # called by Rack server
  def each
    peek, @peek = @peek, nil
    yield peek if peek
    while read(16384, @buf)
      yield @buf
    end
  end

  # non-Rack response body interface
  def read(len = nil, buf = nil)
    raise RuntimeError, 'not #read is compatible with #peek' if @peek
    if @left
      len = @left if len.nil? || len > @left
      ret = @rd.read(len, buf)
      @left -= ret.bytesize if ret
      @left = nil if @left == 0
      ret
    else
      buf&.clear
      len ? nil : ''
    end
  end

  # non-Rack response body interface
  def gets(sep = $/, limit = nil, chomp: false)
    if Integer === sep && limit.nil?
      limit = sep
      sep = $/
    end
    raise RuntimeError, 'not #read is compatible with #peek' if @peek
    return if @left.nil?
    return '' if limit == 0
    limit = @left if limit.nil? || limit > @left
    if limit == 0
      @left = nil
      return nil
    end
    buf = @rd.gets(sep, limit)
    if buf
      @left -= buf.bytesize
      @left = nil if @left == 0

      # we must chomp ourselves for accounting @left
      buf.chomp!(sep) if chomp
    end
    buf
  end

  # we must drain the buffer if the reader aborted prematurely
  def drain
    n = @left or return
    max = 16384
    while n > 0
      len = n > max ? max : n
      @rd.read(len, @buf) and n -= @buf.bytesize
    end
  end
end
