git_manifest = `git ls-files 2>/dev/null`.split("\n")
manifest = File.exist?('MANIFEST') ?
  File.readlines('MANIFEST').map!(&:chomp).delete_if(&:empty?) : git_manifest
if git_manifest[0] && manifest != git_manifest
  tmp = "MANIFEST.#$$.tmp"
  File.open(tmp, 'w') { |fp| fp.puts(git_manifest.join("\n")) }
  File.rename(tmp, 'MANIFEST')
  system('git add MANIFEST')
end

Gem::Specification.new do |s|
  s.name = %q{repobrowse}
  s.version = (ENV['VERSION'] || '0.0.0').dup
  s.homepage = 'https://80x24.org/repobrowse/'
  s.authors = ['repobrowse hackers']
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{repobrowse-public@80x24.org}
  s.files = manifest
  s.summary = File.readlines('README')[0]
  s.test_files = Dir['test/test_*.rb']
  s.add_development_dependency('test-unit', '~> 3.0')

  # hope this gets into
  s.add_dependency('roda', '~> 3.3')
  # TODO: make rugged optional when we start supporting other VCS
  s.add_dependency('rugged', '~> 0.24') # whatever's in Debian stretch
  s.required_ruby_version = '>= 2.3'
  s.licenses = %w(AGPL-3.0+)
end
